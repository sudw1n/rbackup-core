//! The module that handles all of the configuration for `rbackup`.
use crate::error::RBackupError;
use std::{
    collections::HashSet,
    fs,
    io::ErrorKind,
    path::{Path, PathBuf},
};

use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use typed_builder::TypedBuilder;

/// Represents the configuration for `rbackup`.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    #[serde(flatten)]
    pub settings: Settings,
    #[serde(alias = "target")]
    pub targets: Vec<BackupTarget>,
}

/// Represents the settings that apply to all `BackupTarget`s
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Settings {
    pub main: MainSettings,
    pub encrypt: Option<EncryptionSettings>,
}

/// Represents the general options.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MainSettings {
    /// defines where all of the backups will be stored
    pub backup_directory: PathBuf,
}

/// Represents the options for encrypting compressed archives.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EncryptionSettings {
    /// The list of public key fingerprints for whom the archives will be
    /// encrypted. These fingerprints can be retrieved using `gpg`. Run:
    /// ```no_run
    /// gpg --list-keys <recipients>
    /// ```
    /// The output should be something like:
    /// ```ascii_art
    /// pub   ed25519 2022-11-14 [SC]
    ///       4A4826D463241A2C92239B0B587CD77FF5B2C244
    /// uid   [ultimate] Pratik Devkota <sudw1n@proton.me>
    /// sub   cv25519 2022-11-14 [E]
    /// ```
    /// Here, the string `4A4826D463241A2C92239B0B587CD77FF5B2C244` is the
    /// required fingerprint.
    pub recipients: Vec<String>,
}

#[derive(Debug, Clone, TypedBuilder, Serialize, Deserialize)]
pub struct BackupTarget {
    pub name: String,
    pub sources: HashSet<PathBuf>,
    pub exclude: HashSet<String>,
    pub encrypt: bool,
    pub compress: bool,
}

impl Config {
    pub fn start_backup(&self) -> Result<(), RBackupError> {
        // handle each target concurrently
        self.targets
            .par_iter()
            .try_for_each(|target| target.start_backup(&self.settings))?;
        Ok(())
    }
}

impl BackupTarget {
    pub fn start_backup(&self, settings: &Settings) -> Result<(), RBackupError> {
        let backup_directory = &settings.main.backup_directory;
        // create a temporary directory at the root of the backup directory
        let tmp_dir = tempfile::tempdir_in(backup_directory).map_err(|e| RBackupError::Io {
            error: e,
            context: "while creating a temporary directory to copy the files over".to_string(),
        })?;
        // where the backups will be stored for each Target
        let target_dir = backup_directory.join(&self.name);

        // handle copy
        self.sources
            .par_iter()
            .try_for_each(|source| crate::copy::copy(source, tmp_dir.as_ref(), &self.exclude))?;

        create_or_verify_exists(target_dir.as_path())?;

        if self.compress {
            // handle archive
            let mut archive_path = target_dir.join(&self.name);
            archive_path.set_extension("tar.gz");
            crate::archive::archive(&archive_path, Path::new("."), tmp_dir.as_ref())?;

            // handle encryption
            if self.encrypt {
                if let Some(ref encryption_settings) = settings.encrypt {
                    // encrypt the archive and then remove the old (unencrypted) one
                    crate::encrypt::encrypt(&archive_path, &encryption_settings.recipients)?;
                    fs::remove_file(&archive_path).map_err(|e| RBackupError::Io {
                        error: e,
                        context: format!(
                            "while removing the old archive file (\"{}\") after encrypting",
                            archive_path.display()
                        ),
                    })?;
                } else {
                    return Err(RBackupError::ConfigError(format!(
                        "the target \"{}\" has enabled encryption but no encryption settings have been defined",
                        self.name
                    )));
                }
            }
        } else {
            // rename the temporary directory with the target's backup directory
            replace_backup_directory(tmp_dir.as_ref(), &target_dir)?;
        }
        Ok(())
    }
}

fn create_or_verify_exists(target_dir: &Path) -> Result<(), RBackupError> {
    match fs::create_dir(target_dir) {
        Ok(_) => Ok(()),
        Err(ref err) if err.kind() == ErrorKind::AlreadyExists => Ok(()),
        Err(err) => Err(RBackupError::Io {
            error: err,
            context: format!("while creating the directory \"{}\"", target_dir.display()),
        }),
    }
}

fn replace_backup_directory(src_dir: &Path, dest_dir: &Path) -> Result<(), RBackupError> {
    if let Err(e) = fs::remove_dir_all(dest_dir).map_err(|e| match e.kind() {
        ErrorKind::NotFound => Ok(()),
        _ => Err(RBackupError::Io {
            error: e,
            context: "while trying to remove existing backup directory (in order to replace with the latest backup)".to_string(),
        }),
    }) {
        return e;
    }

    fs::rename(src_dir, dest_dir).map_err(|e| RBackupError::Io {
        error: e,
        context: format!(
            "while renaming the directory \"{}\" to \"{}\"",
            src_dir.display(),
            dest_dir.display()
        ),
    })?;

    Ok(())
}
