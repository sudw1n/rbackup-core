//! `rbackup` provides efficient and memory-safe backup functionality for Linux.
//!
//! `rbackup`'s API performs the following tasks:
//! - transfer files recursively from one directory to another
//! - archive a directory using `tar`
//! - compress a directory with various compression algorithms
//! - provide encryption for an archive

pub mod archive;
pub mod config;
pub mod copy;
pub mod encrypt;
pub mod error;
pub mod prelude;
