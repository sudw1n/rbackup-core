//! This module handles all the file copy operations for performing backups.

use glob::Pattern;
use rayon::prelude::{IntoParallelRefIterator, ParallelIterator};
use std::collections::HashSet;
use std::io::ErrorKind;
use std::path::{Path, PathBuf};
use std::{fs, io};

use typed_builder::TypedBuilder;
use walkdir::{DirEntry, Error as WalkDirError, WalkDir};

use crate::error::RBackupError;

/// Copies all of the directories from the absolute path given by `src` to
/// `dest` while ignoring entries matching the glob patterns given by `exclude`.
pub fn copy(src: &Path, dest: &Path, exclude: &HashSet<String>) -> Result<(), RBackupError> {
    let mut exclude_pattern = HashSet::with_capacity(exclude.len());
    for exclusion in exclude.iter().map(|pattern_str| Pattern::new(pattern_str)) {
        let exclusion = exclusion.map_err(RBackupError::ExclusionPattern)?;
        exclude_pattern.insert(exclusion);
    }
    Copy::builder()
        .src(src)
        .dest(dest)
        .exclude(exclude_pattern)
        .build()
        .run()?;
    Ok(())
}

#[derive(TypedBuilder, Debug, Clone)]
struct Copy<'a> {
    src: &'a Path,
    dest: &'a Path,
    exclude: HashSet<Pattern>,
}

impl<'a> Copy<'a> {
    /// Iterates over the given `src` directory excluding entries matching the
    /// `Regex` patterns specified by `exclude`.
    ///
    /// This replicates the structure inside `src` at `dest` by stripping the
    /// `/` prefix.
    ///
    /// In other words, if `src` is `/home/foo/test`, and `dest` is `/home/foo/
    /// bar`, then this will create `/home/foo/bar/home/foo/test/` and copy
    /// all of the contents from `/home/foo/test` to
    /// `/home/foo/bar/home/foo/test/`.
    pub fn run(&self) -> Result<(), RBackupError> {
        // iterate over the directories (recursively)
        // excluding the files matching the given patterns
        // and copy over the entries that are valid files
        for entry in WalkDir::new(self.src)
            .into_iter()
            .filter_entry(|entry| !self.should_exclude_entry(entry))
        {
            let entry = entry.map_err(|e| self.map_walkdir_error(e))?;
            if self.is_valid_file(&entry) {
                self.copy_file(&entry)?;
            }
        }
        Ok(())
    }

    // determine if we should exclude the directory entry
    fn should_exclude_entry(&self, entry: &DirEntry) -> bool {
        let entry_path = entry.path().to_string_lossy();
        self.exclude
            .par_iter()
            .find_any(|pattern| pattern.matches(&entry_path))
            .is_some()
    }

    // convert walkdir::Error into RBackupError
    fn map_walkdir_error(&self, e: WalkDirError) -> RBackupError {
        let error_path = e.path().unwrap_or(self.dest).display();
        let context = format!("while traversing directories at \"{}\"", error_path);
        let error = io::Error::from(e);
        RBackupError::Io { error, context }
    }

    // check if the entry is a valid file
    fn is_valid_file(&self, entry: &DirEntry) -> bool {
        let entry_type = entry.file_type();
        entry_type.is_file()
    }

    // copy over the entry from `src` to `dest` while also creating the necessary
    // directories
    fn copy_file(&self, entry: &DirEntry) -> Result<(), RBackupError> {
        let src_path = entry.path();
        let dest_path = self.build_destination_path(src_path)?;

        if let Some(parent) = dest_path.parent() {
            self.create_directory_all(parent)?;
        }

        fs::copy(src_path, &dest_path).map_err(|e| {
            let context = format!(
                "while copying file \"{}\" to \"{}\"",
                src_path.display(),
                dest_path.display()
            );
            RBackupError::Io { error: e, context }
        })?;
        Ok(())
    }

    // the destination paths will be constructed by stripping the `/` from the
    // source path so
    // `src`: `/home/foo/Documents/` and
    // `dest`: `/home/foo/Backups/test/` becomes
    // `/home/foo/Backups/test/home/foo/Documents`.
    fn build_destination_path(&self, src_path: &Path) -> Result<PathBuf, RBackupError> {
        let dest_path = PathBuf::from(self.dest).join(
            src_path
                .strip_prefix("/")
                .expect("should be able to strip the '/' from an absolute path"),
        );
        Ok(dest_path)
    }

    // create all directories at the given `parent` ignoring the error if the
    // directory already exists
    fn create_directory_all(&self, parent: &Path) -> Result<(), RBackupError> {
        if let Err(e) = fs::create_dir_all(parent).map_err(|e| match e.kind() {
            ErrorKind::AlreadyExists => Ok(()),
            _ => {
                let context = format!("while creating directories at \"{}\"", parent.display());
                Err(RBackupError::Io { error: e, context })
            }
        }) {
            return e;
        }
        Ok(())
    }
}
