#[derive(thiserror::Error, Debug)]
pub enum RBackupError {
    #[error("An I/O error occurred {context}: {error}")]
    Io {
        #[source]
        error: ::std::io::Error,
        context: String,
    },
    #[error("Invalid pattern given for exclusion: {0}")]
    ExclusionPattern(#[source] glob::PatternError),
    #[error("GPG error {context}: {error}")]
    Gpg {
        #[source]
        error: gpgme::Error,
        context: String,
    },
    #[error("Invalid path given: {msg}")]
    BadPath { msg: String },
    #[error("Invalid configuration: {0}")]
    ConfigError(String),
    #[error("An internal bug occurred, please report this")]
    Internal,
}
