//! This module handles creating archives and compressing them.
use flate2::write::GzEncoder;
use flate2::Compression;
use std::fs::File;
use std::path::Path;
use tar::Builder;

use crate::error::RBackupError;

/// Creates an archive from the given `source_dir` with the
/// `path_inside_archive` as the path of the directory inside the archive and
/// save the archive with the path `archive_filename`.
pub fn archive(
    archive_filename: &Path,
    path_inside_archive: &Path,
    source_dir: &Path,
) -> Result<(), RBackupError> {
    let tar_gz = File::create(archive_filename).map_err(|e| RBackupError::Io {
        error: e,
        context: format!(
            "Failed to create the archive (\"{}\")",
            archive_filename.display()
        ),
    })?;
    let gz_encoder = GzEncoder::new(tar_gz, Compression::best());
    let mut tar_builder = Builder::new(gz_encoder);

    tar_builder
        .append_dir_all(path_inside_archive, source_dir)
        .map_err(|e| RBackupError::Io {
            error: e,
            context: format!(
                "Failed to append \"{}\" to the archive (\"{}\"",
                source_dir.display(),
                archive_filename.display()
            ),
        })?;

    tar_builder.finish().map_err(|e| RBackupError::Io {
        error: e,
        context: format!(
            "Failed to finish writing the archive (\"{}\"",
            archive_filename.display()
        ),
    })?;

    Ok(())
}
