//! This module handles encryption of archive files.

use std::{
    fs::File,
    path::{Path, PathBuf},
};

use gpgme::{Context, Key, Protocol};

use crate::error::RBackupError;

/// Encrypt a file given by the `file_name` for the recipients whose public key
/// fingerprint are given in `recipients`
pub fn encrypt(file_name: &Path, recipients: &[String]) -> Result<(), RBackupError> {
    let mut encrypter = Encrypter::new(recipients)?;
    let mut output_path = PathBuf::from(file_name);
    if !output_path.set_extension("gpg") {
        return Err(RBackupError::BadPath {
            msg: format!(
                "the path \"{}\" doesn't have an extension",
                file_name.display()
            ),
        });
    }
    encrypter.encrypt_file(file_name.as_ref(), &output_path)
}

/// Struct representing the encryption handler.
pub struct Encrypter {
    context: Context,
    keys: Vec<Key>,
}

impl Encrypter {
    /// Creates a new `Encrypter` instance.
    pub fn new(recipients: &[String]) -> Result<Self, RBackupError> {
        let mut context =
            Context::from_protocol(Protocol::OpenPgp).map_err(|e| RBackupError::Gpg {
                error: e,
                context: format!("while initializing the encryption context"),
            })?;
        let keys = if !recipients.is_empty() {
            context
                .find_keys(recipients)
                .map_err(|e| RBackupError::Gpg {
                    error: e,
                    context: format!("while finding keys from the given recipients"),
                })?
                .filter_map(|x| x.ok())
                .filter(|k| k.can_encrypt())
                .collect()
        } else {
            Vec::new()
        };

        Ok(Self { context, keys })
    }

    /// Encrypts the specified file using GPG encryption.
    pub fn encrypt_file(
        &mut self,
        file_path: &Path,
        output_path: &Path,
    ) -> Result<(), RBackupError> {
        let input_file = File::open(file_path).map_err(|e| RBackupError::Io {
            error: e,
            context: format!(
                "while opening the input file for encryption \"{}\"",
                file_path.display()
            ),
        })?;
        let mut output_file = File::create(output_path).map_err(|e| RBackupError::Io {
            error: e,
            context: format!(
                "while creating the output file for encryption \"{}\"",
                output_path.display()
            ),
        })?;

        self.context
            .encrypt(&self.keys, input_file, &mut output_file)
            .map_err(|e| RBackupError::Gpg {
                error: e,
                context: format!(
                    "while encrypting the input file \"{}\" to \"{}\"",
                    file_path.display(),
                    output_path.display()
                ),
            })?;

        Ok(())
    }
}
